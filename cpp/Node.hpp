//
//  Node.h
//  K1.alexeit.array2tree
//
//  Created by Alexei Tchervinsky on 18/11/2018.
//  Copyright © 2018 Alexei Tchervinsky. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <string>
#include <memory>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stack>

#include "Settings.hpp"
#include "Common.hpp"

namespace alexeit {
  typedef unsigned int uint;

  class Node
  // https://stackoverflow.com/questions/11711034/stdshared-ptr-of-this
  : public std::enable_shared_from_this<Node> {
  public:
    // constructor with level and word and fake/actual, no reference to parent

    Node(std::string word, uint level, bool fake = false) :
    level_(level + 1),
    word_(word),
    fake_(fake)
    {
    }

    match_t match_chain(std::vector<std::string> & pattern);

    //node_t *node_new(void *data, node_t *parent);
    //void node_init(node_t *instance, void *data, node_t *parent);

    void dump_chain();


    ~Node() = default;

    // search in chain of data
    std::shared_ptr<Node> search_chain(std::vector<std::string> & pattern);


    void data_insert_chain(std::vector<std::string> & pattern);

    retcmd_t search_for_shortcut_chain(std::vector<std::string> &shortcut);

    std::string & getWord()
    {
      return word_;
    }

    uint getLevel()
    {
      return level_;
    }

    std::vector<std::shared_ptr<Node>> * getTree()
    {
      return &tree_;
    }
    // https://en.cppreference.com/w/cpp/memory/enable_shared_from_this

    std::shared_ptr<Node> getptr()
    {
      return shared_from_this();
    }

    // has children ?

    bool hasChildren()
    {
      return(tree_.size()) ? true : false;
    }
  private:
    uint level_;
    std::string word_;
    bool fake_;
    std::vector<std::shared_ptr<Node>> tree_;
  }; // Node

  void dump_node(std::shared_ptr<alexeit::Node> node);

} // namespace


#endif /* Node_hpp */
