//
//  main.cpp
//  K1.alexeit.array2tree
//
//  Created by Alexei Tchervinsky on 18/11/2018.
//  Copyright © 2018 Alexei Tchervinsky. All rights reserved.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include <algorithm>
#include <iterator>

#include "Settings.hpp"
#include "Common.hpp"

#include "Node.hpp"

namespace alexeit {

  template <typename T>
  void printVector(const std::vector<T> vec)
  {
    for (auto i = vec.begin(); i != vec.end(); i++) {
      std::cout << *i << std::endl;
    }
  } // printVector

  // https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/

  std::vector<std::string> splitString(const std::string input_string)
  {
    std::istringstream iss(input_string);

    std::vector<std::string>out_vec(std::istream_iterator<std::string>{iss},
    std::istream_iterator<std::string>());

    return out_vec;
  }

  /**
   * explanation string of retcmd_t return code
   */
  std::string rct_rc_explain(retcode_t rc, bool long_expl)
  {
    if (SHORTCUT_FOUND == rc) {
      return(long_expl) ? "% Success: shortcut found" : "found";
    } else
      if (SHORTCUT_FOUND_FAKE == rc) {
      return(long_expl) ? "% Warning: shortcut found fake" : "fake";
    } else
      if (SHORTCUT_FOUND_LONG == rc) {
      return(long_expl) ? "% Warning: shortcut found, but is longer" : "long";
    } else
      if (SHORTCUT_AMBIGUOUS == rc) {
      return(long_expl) ? "% Error: shortcut ambiguous" : "amb";
    } else
      if (SHORTCUT_NOT_FOUND == rc) {
      return(long_expl) ? "% Error: shortcut not found" : "not";
    }
    return(long_expl) ? "Unknown error" : "unkn";
  }

} // namespace

int main(int argc, const char * argv[])
{
  // insert code here...
  std::cout << "Prefix tree" << std::endl;
  std::vector<std::string> vec_test;

  // 2019-01-01 read data from argv
  std::ifstream ifs;
  std::string input_string;
  if (argc >= 2)
  {
    ifs.open(argv[1], std::ios::in);
  }
  else
  {
    std::cout << "Error: no input filename in command line" << std::endl;
    return 1;
  }

  while (std::getline(ifs, input_string)) {
    if (COMMENT_SIGN == input_string[0]) { // line is commented
      continue;
    }
    vec_test.push_back(input_string);
  }
  std::sort(vec_test.begin(), vec_test.end());

  // root node
  std::shared_ptr<alexeit::Node>root = std::make_shared<alexeit::Node>("", -1);

  // create tree
  for (auto i = vec_test.begin(); i != vec_test.end(); i++) {
    //auto n = alexeit::words_number_of(*i);
    std::vector<std::string> node_data = alexeit::splitString(*i);

    root->data_insert_chain(node_data);
  }
  alexeit::dump_node(root);

  // enter user string with shortcuts
  std::string input_command;
  std::string prompt = "enter command (Ctrl-D) to exit>";

  while(true)
  {
    std::cout << prompt;
    std::cout.flush();
    if (!std::getline(std::cin, input_command))
    {
      break;
    }
    if (!input_command.size())
    {
      continue;
    }
    std::vector<std::string> shortcut = alexeit::splitString(input_command);
    alexeit::retcmd_t rct;
    rct.rc = alexeit::SHORTCUT_NOT_FOUND;
    rct = root->search_for_shortcut_chain(shortcut);
    std::cout << alexeit::rct_rc_explain(rct.rc, false) << "\n";
  }

  return 0;
}
