//
//  Node.cpp
//  K1.alexeit.array2tree
//
//  Created by Alexei Tchervinsky on 18/11/2018.
//  Copyright © 2018 Alexei Tchervinsky. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <stdexcept>

#include "Node.hpp"

namespace alexeit {

  // search in chain of data
  // breadth first search

  std::shared_ptr<Node> Node::search_chain(std::vector<std::string> & pattern)
  {
    std::shared_ptr<Node> ret = nullptr;

    for (auto i = tree_.begin(); i != tree_.end(); i++) {
      if (0 == (*i)->word_.compare(pattern[level_])) {
        ret = (*i)->search_chain(pattern); // recursive search
      }
    } /* for */
    // not found chain, add to this level
    if (nullptr == ret) {
      ret = shared_from_this();
    }
    return ret;
  } // search_chain

  void  Node::data_insert_chain(std::vector<std::string> & pattern)
  {
    std::shared_ptr<Node> node_to_which_insert = nullptr;

    while (true) {
      node_to_which_insert = search_chain(pattern);
      uint level = node_to_which_insert->getLevel();
      bool fake = (level == (pattern.size() - 1)) ? false : true;
      node_to_which_insert->tree_.push_back(std::make_shared<Node>(pattern[level], level, fake));
      if (level == pattern.size() - 1) {
        break;
      }
    } // while
  } // data_insert_chain


    /**
   */
  match_t Node::match_chain(std::vector<std::string> & pattern)
  {
    match_t rcm;

    rcm.match = false;
    rcm.exact = false;
    rcm.longer = false;
    rcm.fake = false;

    if (0 == word_.compare(pattern[level_ - 1])) { // first exact match
      rcm.match = true;
      rcm.exact = true;
      rcm.node = shared_from_this();
    } else {
      if (0 == word_.compare(0, pattern[level_ - 1].size(),
        pattern[level_ - 1])) {
        rcm.match = true;
        rcm.node = shared_from_this();
      }
    }
    if (pattern.size() > level_) {
      rcm.longer = true;
    }

    if (true == fake_) {
      rcm.fake = true;
    }

    return rcm;
  }


  void Node::dump_chain()
  {
    std::cout << std::endl;
    std::cout << "lev: " << level_ << ", word: " << word_ << ", a/f: " << (fake_ ? "f" : "a") << std::endl;

    if (0 == tree_.size()) {
      return;
    }
    for (auto i = tree_.begin(); i != tree_.end(); i++) {
      (*i)->dump_chain(); // recursive dump
    }
    std::cout << std::endl;
  } // dump_chain

  /**
    dump without recursion
  * https://www.geeksforgeeks.org/preorder-traversal-of-n-ary-tree-without-recursion/
  */
  //std::vector<std::shared_ptr<Node>>
  void dump_node(std::shared_ptr<Node> node)
  {
    if (nullptr == node)
    {
      throw std::invalid_argument("dump_node argument is nullptr");
    }

#define USE_VECTOR_AS_STACK // use vector as stack

#ifdef USE_VECTOR_AS_STACK
    std::vector<std::shared_ptr<Node>> nodes;
#else // use stack
    std::stack<std::shared_ptr<Node>> nodes;
#endif

#ifdef USE_VECTOR_AS_STACK
    nodes.push_back(node);
#else
    nodes.push(node);
#endif


    while(!nodes.empty())
    {
#ifdef USE_VECTOR_AS_STACK
      std::shared_ptr<Node> node = nodes.back();
      nodes.pop_back();
#else
      std::shared_ptr<Node> node = nodes.top();
      nodes.pop();
#endif
      std::cout << std::endl;
      std::cout << "lev: " << node->getLevel() << ", word: " << node->getWord() << std::endl;

// We must store children from right to left so that leftmost node is popped first
      std::vector<std::shared_ptr<Node>>::iterator it = node->getTree()->end();

      while (it != node->getTree()->begin())
      {
        --it;
#ifdef USE_VECTOR_AS_STACK
        nodes.push_back(*it);
#else
        nodes.push(*it);
#endif
      } // while inner
    } // while outer
  }

  /**
   */


  retcmd_t Node::search_for_shortcut_chain(std::vector<std::string> &shortcut)
  {
    retcmd_t rct;
    if (tree_.size()) {

      std::vector<match_t> match_vec;

      for (auto node : tree_) { // scan branches

        match_t rcm;

        rcm = node->match_chain(shortcut); // try match with shortcut

        if (rcm.match) { // match ok
          match_vec.push_back(rcm); // remember it
          if (rcm.exact) { // exact match found
            break; // FIXME: is it correct? we find first suitable
          }
        }

      } // for

      switch (match_vec.size()) { // analyze matches

      case 0:
      {// no matches found
        rct.rc = SHORTCUT_NOT_FOUND;
        break;
      }

      case 1:
      { // only one match
        match_t rcm = match_vec[0];
        if (rcm.longer) { // shortcut is longer, explore this node
          if (rcm.node->hasChildren()) { // node has the children, try
            rct.node_cmd_vec.push_back(rcm.node); // remember in path
            rct = rcm.node->search_for_shortcut_chain(shortcut);
          } else { // long shortcut, inform user
            rct.rc = SHORTCUT_FOUND_LONG;
            rct.node_cmd_vec.push_back(rcm.node);
          }
        } else { // exact match
          if (rcm.fake) {
            rct.rc = SHORTCUT_FOUND_FAKE; // fake node (e.g. command without action)
            rct.node_cmd_vec.push_back(rcm.node); // return this node to caller
            //rcm.node->find_nearest_actual_nodes_chain(rct); // find all nearest actual nodes

          } else {
            rct.rc = SHORTCUT_FOUND;
            rct.node_cmd_vec.push_back(rcm.node); // return this node to caller
          }
          // return this node to caller
        }
        break;
      }

      default:
      { // more than one matches: need more processing
        rct.rc = SHORTCUT_AMBIGUOUS;
        break;
      }
      } // switch
    }// if
    else {
      rct.rc = SHORTCUT_NOT_FOUND; // no branches, no match
    }
    return rct;
  } // search_for_shortcut_chain()
} // namespace
