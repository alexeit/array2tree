//
//  Common.hpp
//  K1.alexeit.array2tree
//
//  Created by Alexei Tchervinsky on 15/01/2019.
//  Copyright © 2019 Alexei Tchervinsky. All rights reserved.
//

#ifndef Common_h
#define Common_h

#include <iostream>
#include <vector>

namespace alexeit {

  // comment line in input file
#define COMMENT_SIGN '#'
  // forward declaration
  class Node;

  enum retcode_t {
    SHORTCUT_FOUND = 0,
    SHORTCUT_FOUND_LONG = 1,
    SHORTCUT_FOUND_FAKE = 2,
    SHORTCUT_AMBIGUOUS = 3,
    SHORTCUT_NOT_FOUND = 4
  };

  class Node; // forward declaration

  typedef struct retcmd_s {
    retcode_t rc;
    std::vector<std::shared_ptr<Node>> node_cmd_vec;
  } retcmd_t;

  // return from compare

  typedef struct match_s {
    bool match; // true match, false not match
    bool exact; // exact match
    bool longer; // true pattern is longer than this (word count)
    bool fake; // the node of match is fake node (e.g, command has no action)
    std::shared_ptr<Node> node; // node if match is true
  } match_t;

  /**
   * explanation string of retcmd_t return code
   */
  std::string rct_rc_explain(retcode_t rc, bool long_expl = true);
} // namespace
#endif /* Common_h */
