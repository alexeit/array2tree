// 2019-02-03 alexeit rust meetup, node struct 12:49
// 2019-02-17 alexeit rust meetup in "Times" 12:00 - 14:00
// 2019-09-14 alexeit at home linked lists: https://rust-unofficial.github.io/too-many-lists/

// fumlead@gmail.com Ilya Bogdanov @vitvakatu

use std::ptr;

pub struct Node {
    level_: usize,
    word_: String,
    fake_: bool,
    tree_: std::vec::Vec<Node>,
}

impl Node {
    pub fn build_node(word: String, level: usize, fake: bool) -> Node {
        Node {
            level_: if level == 1000 { 0 } else { level + 1 },
            word_: word,
            fake_: fake,
            tree_: vec![],
        }
    }

    fn get_level(self_node: *mut Node) -> usize
    {
        unsafe {
            return (*self_node).level_;
        }
    }

    fn add_node(self_node: *mut Node, node: Node)
    {
        unsafe {
            (*self_node).tree_.push(node);
        }
    }

    fn search_chain(&mut self, pattern: &[String]) -> *mut Node
    {
        let mut ret: *mut Node = ptr::null_mut();
        let p_self: *mut Node = self;
        let mut i = 0;
        while i < self.tree_.len() {
            let node = &mut self.tree_[i];
            if node.word_ == pattern[self.level_] {
                ret = node.search_chain(pattern); // recursion
            }
            i += 1;
        }
        if ret.is_null() {
            return p_self;
        } else {
            return ret;
        }
    } // search_chain

    pub fn data_insert_chain(&mut self, pattern: &[String])
    {
        loop {
            let node_to_which_insert = self.search_chain(&pattern);
            let level: usize = Node::get_level(node_to_which_insert);
            let fake: bool = if level == (pattern.len() - 1) { false } else { true };
            let node = Node::build_node(pattern[level].clone(), level, fake);
            Node::add_node(node_to_which_insert, node);
            if level == (pattern.len() - 1) {
                break;
            }
        } // while
    }

    // 2020-01-06 since I am not sure if this is actually needed, this code is for experiment
    // with raw pointers
    // I will remove all leaves which word is "x"
    // I will try to work with vector or iterator
    pub fn data_remove_chain_pointer(self_node: *mut Node)
    {
        let tree_iter;
        unsafe {
            tree_iter = (*self_node).tree_.iter_mut();
        } // end of unsafe
        for t in tree_iter {
            if t.tree_.len() != 0 { // if has children, scan them
                let p_node: *mut Node = t;
                Node::data_remove_chain_pointer(p_node); // recursion with raw pointer
            } else {
                unsafe {
                    (*self_node).tree_.retain(|t| {
                        if t.word_ == "x".to_string() { false } else { true }
                    })
                } // end of unsafe
            }
        }
    }

    pub fn data_remove_chain(&mut self)
    {
        let p_self: *mut Node = self;
        Node::data_remove_chain_pointer(p_self);
    }

    pub fn dump_chain(&self)
    {
        let x = self;
        println!("{l} {lv} {w} {wv} {f} {fv}", l = "lev: ", lv = x.level_, w = ", word: ", wv = x.word_, f = ", a/f: ", fv = if x.fake_ { "f" } else { "a" });

        let z = &x.tree_;
        let tree_iter = &mut z.iter();

        for t in tree_iter {
            t.dump_chain(); // recursion
        }
    }
} // impl Node
