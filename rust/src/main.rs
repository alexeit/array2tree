// 2019-02-02 alexeit port array2tree from C++ to rust

extern crate core;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, BufReader};
use node::*;

mod node; // FIXME: Vlad B 2019-02-03

fn main() -> io::Result<()> {
    println!("array2tree!");

    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);

    let ifs = File::open(&args[1]).expect("file in not found");
    let ifb = BufReader::new(ifs);

    let mut vec_test = Vec::new();

    for line in ifb.lines() {
        let u = line.unwrap();
        println!("{}", u);
        vec_test.push(u);
    }

    vec_test.sort();

    // create root node
    let mut root = Node::build_node("".to_string(), 1000, false);
    // build tree
    for line in vec_test {
        let v: Vec<String> = line.split_whitespace().map(|s| s.to_string()).collect();
        root.data_insert_chain(&v);
    }

    println!("{}", "tree before remove");
    root.dump_chain();
    root.data_remove_chain();
    println!("{}", "tree after remove");
    root.dump_chain();

    Ok(())
}
